# Contact Us Form Authentication
This project is a backend application consisting of a RESTful JSON API for a contact us form. 
All persistent data is to be stored in a simple JSON file that’s operated on through Node’s fs module.

## Installation
1. Run `npm install` to install dependencies
2. Create a file called `.env`, and enter the following variables, values:
```bash
PORT=5000
JWT_SECRET=YourSecretKeyPlease
USERS_FILE_LOCATION=./data/user.json
CONTACTS_FILE_LOCATION=./data/entry.json
```
3. Create an empty folder in the src level and name it "data"
4. Run `npm run dev` to begin the nodemon instance
