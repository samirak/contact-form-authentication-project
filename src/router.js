import express from 'express'
import entryRouter from './routes/entry'
import userRouter from './routes/user'
import authRouter from './routes/authentication'


const router = express.Router()

router.use('/contact_form', entryRouter)
router.use('/users', userRouter)
router.use('/auth', authRouter)

export default router