export default (req, res, next) => {
  const requiredUserProps = ["name", "password", "email"]
  const errors = { message: 'validation error', invalid: [] }
  const emailFormat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  const passwordFormat = /[a-zA-Z0-9]{8,}$/ //Minimum 8 characters
  const newUser = req.body

  if ( newUser.hasOwnProperty('email') ) {
    if ( !emailFormat.test(newUser.email) ) {      
      errors.invalid.push('email')      
    }
  }

  if ( newUser.hasOwnProperty('password') ) {
    if ( !passwordFormat.test(newUser.password) ) {
      errors.invalid.push('password')
    }   
  }  

  requiredUserProps.forEach(property => {
    if ( !newUser.hasOwnProperty(property) ) {
      errors.invalid.push(`${property}`)
    }
  })
  
  console.log(Object.values(errors))
     
  if ( errors.invalid.length > 0 ) {
    return res.status(400).json(errors)
  }  
  
  next()
}