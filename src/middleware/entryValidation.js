export default (req, res, next) => {
  const requiredEntryProps = ["name", "email", "phoneNumber", "content"]
  const errors = { message: 'validation error', invalid: [] }
  const emailFormat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  const newEntry = req.body
  
  if ( newEntry.hasOwnProperty('email') ) {
    if ( !emailFormat.test(newEntry.email) ) {      
      errors.invalid.push('email')      
    }
  }

  requiredEntryProps.forEach(property => {
    if ( !newEntry.hasOwnProperty(property) ) {
      errors.invalid.push(`${property}`)
    }
  })
  
  console.log(Object.values(errors))
  
  if ( errors.invalid.length > 0 ) {
    return res.status(400).json(errors)
  }
  
  next()
}