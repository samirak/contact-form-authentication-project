export default (err, req, res, next) => {
  if (res.headersSent) {
    return next(err)
  }
  console.error(err.stack)
  return res.status(500).send({error: "Unexpected error found"})
}