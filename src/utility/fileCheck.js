import fs from 'fs'
import path from 'path'

const usersFile = path.resolve(process.env.USERS_FILE_LOCATION)
const entryFile = path.resolve(process.env.CONTACTS_FILE_LOCATION)

// Checks if the file exists
const checkFileExists = (file) => {
  try {
    fs.accessSync(file, fs.constants.R_OK | fs.constants.W_OK)
    return true
  } 
  catch (err) {
    return false
  }
}

// Creates user.json file with an empty array inside
const userFileCheck = () => {
  if (checkFileExists(usersFile) === false) {
    fs.writeFile(usersFile, JSON.stringify([]), { flag: 'wx' }, function (err) {
      if (err) throw err;
      console.log("user.json file got created!");
    })          
  } 
  return
}

// Creates entry.json file with an empty array inside
const entryFileCheck = () => {
  if (checkFileExists(entryFile) === false) {
    fs.writeFile(entryFile, JSON.stringify([]), { flag: 'wx' }, function (err) {
      if (err) throw err;
      console.log("user.json file got created!");
    })          
  } 
  return
}

export { userFileCheck, entryFileCheck }