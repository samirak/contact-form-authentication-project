import {promises as fs } from 'fs'
import path from 'path'
//import * as file from './fileCheck'

const usersFile = path.resolve(process.env.USERS_FILE_LOCATION)
const entryFile = path.resolve(process.env.CONTACTS_FILE_LOCATION)


// Reading from user.json 
const getAllUsers = async () => {
  try {
    let content = await fs.readFile(usersFile)   
    return JSON.parse(content) 
  } 
  catch (err) {
    console.error("getAllUsers Module Error", err)
    throw err
  }
}


// Reading from contacts.json
const getAllContacts = async () => {
  try {           
    let content = await fs.readFile(entryFile)  
    return JSON.parse(content)    
  }
  catch (err) {
    console.error("getAllContacts function Error", err)
    throw err
  }  
}

// Writing to users.json
const writeUsers = async (data) => {
  try {
    await fs.writeFile(usersFile, JSON.stringify(data))
  } 
  catch (err) {
    console.error("Error in writeUsers function", err)
    throw err
  } 
}

// Writing to contacts.json
const writeContacts = async (data) => {
  try {
    await fs.writeFile(entryFile, JSON.stringify(data))
  }
  catch (err) {
    console.error("Error in writeContacts function", err)
    throw err
  }  
}

// Adding new user
const addUser = async (data) => {
  try {   
    let content = await getAllUsers()
    content.push(data)    
    await writeUsers(content)
    console.log("Users file got written")
  } 
  catch (err) {
    console.error("Error in addUser function", err)
    throw err
  }
}

// Adding new contact
const addContact =  async (data) => {
  try {
    let content = await getAllContacts()
    content.push(data)
    await writeContacts(content)
    console.log("Contacts file got written")
  } 
  catch (err) {
    console.error("Error in addContact function", err)
    throw err
  }
}

const getItemById = async (id) => {
  try {
    const entryList = await getAllContacts()
    const entryIndex = entryList.findIndex(entry => entry.id == id)

    if (entryIndex != -1) {
      return entryList[entryIndex]
    }
    return false
  }
  catch (err) {
    console.error("Error in find item by id function", err)
    throw err
  }
}

export {
  getAllContacts, 
  getAllUsers,
  addContact,
  addUser,
  getItemById
}