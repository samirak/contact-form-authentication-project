import express from 'express'
import * as jwtGenerator from 'jsonwebtoken'
import bcrypt from 'bcrypt'
import * as db from '../utility/dbHandler'

const router = express.Router()


// Part 3 - Route to log a registered user in to create a JWT (JSON Web Token) token: POST /auth
router.post('', async (req, res, next) => {
  try {
    const reqEmail = req.body.email
    const usersList = await db.getAllUsers() 
    const userIndex = usersList.findIndex( user => user.email == reqEmail )
    const match = await bcrypt.compare(req.body.password, usersList[userIndex].password);

    if (userIndex != -1 && match) {      
      const token = jwtGenerator.sign({reqEmail}, process.env.JWT_SECRET, {expiresIn: '5m'})
      return res.status(201).send({token})      
    }  
    res.status(401).send( { error: "Incorrect credentials provided" } )
  }
  catch (err) {
    console.error('Error in creating a JWT token route', err)
    return next(err)
  }
})




export default router