import express from 'express'
import { v4 as uuidv4 } from 'uuid'
import jwtVerification from '../middleware/jwtVerification'
import entryValidation from '../middleware/entryValidation'
import * as db from '../utility/dbHandler'
import * as file from '../utility/fileCheck'

const router = express.Router()


// Part 1 - Route to create an entry when the user submits their contact form: POST /contact_form/entries
router.post("/entries", entryValidation, async (req, res, next) => {
  try {      
    // Checking if the entry.json file exists if not get created 
    file.entryFileCheck()   

    const newContact = { id: uuidv4(), ...req.body }  
        
    await db.addContact(newContact)
    return res.status(201).json(newContact)
  } 
  catch (err) {
    console.error('Error in creating a new entry route', err)
    return next(err)
  }  
})

// router.use(jwtVerification)

// Part 4: Route to get a listing of all submissions when given a valid JWT is provided
router.get('/entries', jwtVerification, async (req, res, next) => {
  try {
    // Checking if the entry.json file exists if not get created 
    file.entryFileCheck()   

    return res.status(200).json(await db.getAllContacts())
  } 
  catch (err) {
    console.error('Error in creating a new entry route', err)
    return next(err)
  }
})


// Part 5: Route to get a specific submission when given an ID alongside a valid JWT
router.get('/entries/:id', jwtVerification, async (req, res, next) => {
  try {
    // Checking if the entry.json file exists if not get created 
    file.entryFileCheck()   

    const reqID = req.params.id
    const matchedItem = await db.getItemById(reqID)
    
    if (matchedItem) {
      return res.status(200).json(matchedItem) 
    }
    return res.status(404).send({message: `entry ${reqID} not found`})       
  }
  catch (err) {
    console.error('Error in getting a specific submission with ID and JWT route', err)
    return next(err)
  } 
})


export default router