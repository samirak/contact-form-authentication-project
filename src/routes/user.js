import express from 'express'
import { v4 as uuidv4 } from 'uuid'
import bcrypt from 'bcrypt'
import userValidation from '../middleware/userValidation'
import * as db from '../utility/dbHandler'
import * as file from '../utility/fileCheck'

const saltRounds = 10
const router = express.Router()


// Part 2 - Route to create a user: POST /users
router.post('', userValidation, async (req, res, next) => {
  // Checking if the user.json file exists if not get created 
  file.userFileCheck()
  
  const id = uuidv4()
  const name = req.body.name
  const email = req.body.email
  const password = req.body.password
  const newUser = { id, name, email, password }

  bcrypt.hash(password, saltRounds, async (err, hash, next) => {
    try {
      newUser.password = hash
      await db.addUser(newUser)
      return res.status(201).json({ id, name, email })
    } 
    catch (err) {
      console.error("Error in creating new user route!", err)
      return next(err)
    }        
  })
})



export default router