import express from 'express'
import router from './src/router'
import errorHandler from './src/middleware/errorHandler'
import cors from 'cors'

const port = process.env.PORT || 3000
const app = express()

app.use(express.json())
app.use(cors())

app.use(router)

app.use(errorHandler)  

app.listen(port, () =>
  console.log(`API server is listening port: ${port}`)
)
